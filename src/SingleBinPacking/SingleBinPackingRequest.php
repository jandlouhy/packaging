<?php

declare(strict_types=1);

namespace App\SingleBinPacking;

use App\DataObject\Product;
use Symfony\Component\Validator\Constraints as Assert;

final class SingleBinPackingRequest
{
    /**
     * @var Product[]
     * @Assert\NotBlank(message="At least one product has to be specified.")
     * @Assert\Valid()
     */
    private array $products;

    /** @param Product[] $products */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /** @return Product[] */
    public function getProducts(): array
    {
        return $this->products;
    }
}
