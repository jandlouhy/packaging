<?php

declare(strict_types=1);

namespace App\SingleBinPacking;

use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class SingleBinPackingRequestFactory
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function create(Request $request): SingleBinPackingRequest
    {
        $products = $this->serializer->deserialize(
            $request->getContent(),
            'App\DataObject\Product[]',
            JsonEncoder::FORMAT
        );

        $request = new SingleBinPackingRequest($products);

        $violations = $this->validator->validate($request);
        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }

        return $request;
    }
}
