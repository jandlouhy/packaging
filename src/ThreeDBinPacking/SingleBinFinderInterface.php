<?php

namespace App\ThreeDBinPacking;

use App\DataObject\Bin;
use App\SingleBinPacking\SingleBinPackingRequest;

interface SingleBinFinderInterface
{
    public function find(SingleBinPackingRequest $request): Bin;
}
