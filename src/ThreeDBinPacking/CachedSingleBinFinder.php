<?php

declare(strict_types=1);

namespace App\ThreeDBinPacking;

use App\DataObject\Bin;
use App\DataObject\Product;
use App\SingleBinPacking\SingleBinPackingRequest;
use Doctrine\Common\Cache\Cache;

final class CachedSingleBinFinder implements SingleBinFinderInterface
{
    private SingleBinFinder $singleBinFinder;

    private Cache $cache;

    public function __construct(SingleBinFinder $singleBinFinder, Cache $cache)
    {
        $this->singleBinFinder = $singleBinFinder;
        $this->cache = $cache;
    }

    public function find(SingleBinPackingRequest $request): Bin
    {
        $cacheKey = $this->getCacheKey($request);

        if ($this->cache->contains($cacheKey)) {
            return $this->cache->fetch($cacheKey);
        }

        $bin = $this->singleBinFinder->find($request);
        $this->cache->save($cacheKey, $bin);

        return $bin;
    }

    private function getCacheKey(SingleBinPackingRequest $request): string
    {
        $productsKeys = array_map(function(Product $product) {
            return serialize($product);
        }, $request->getProducts());

        sort($productsKeys);

        return serialize($productsKeys);
    }
}
