<?php

declare(strict_types=1);

namespace App\ThreeDBinPacking;

use App\DataObject\Product;
use App\Exception\ValidationException;
use App\SingleBinPacking\SingleBinPackingRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Client
{
    private const API_ADDRESS = 'http://eu.api.3dbinpacking.com/packer/';
    private const SINGLE_BIN_PACKING = 'pack';

    private string $username;

    private string $apiKey;

    /** @var array<string, string|mixed> */
    private array $bins;

    private HttpClientInterface $client;

    private LoggerInterface $logger;

    /** @param array<string, string|mixed> $bins */
    public function __construct(
        string $username,
        string $apiKey,
        array $bins,
        HttpClientInterface $client,
        LoggerInterface $logger
    ) {
        $this->username = $username;
        $this->apiKey = $apiKey;
        $this->bins = $bins;
        $this->client = $client;
        $this->logger = $logger;
    }

    public function calculateSingleBinPacking(SingleBinPackingRequest $request): array
    {
        $products = array_map(function (Product $product) {
            return $product->toArray();
        }, $request->getProducts());

        $response = $this->client->request(
            Request::METHOD_POST,
            self::API_ADDRESS . self::SINGLE_BIN_PACKING,
            [
                'json' => [
                    'username' => $this->username,
                    'api_key' => $this->apiKey,
                    'bins' => $this->bins,
                    'items' => $products,
                ]
            ]
        )->toArray();

        $data = $response['response'];

        if ($data['status'] === 0) {
            $this->logger->critical(
                'Error while finding single bin packing',
                [
                    'errors' => $data['errors'],
                    'products' => $products,
                    'bins' => $this->bins,
                ]
            );
        }

        if ($data['errors'] !== []) {
            throw new ValidationException($data['errors']);
        }

        return $data['bins_packed'];
    }
}
