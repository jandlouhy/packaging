<?php

declare(strict_types=1);

namespace App\ThreeDBinPacking;

use App\DataObject\Bin;
use App\Exception\SingleBinNotFound;
use App\SingleBinPacking\SingleBinPackingRequest;
use Psr\Log\LoggerInterface;

final class SingleBinFinder implements SingleBinFinderInterface
{
    private Client $client3dbp;

    private LoggerInterface $logger;

    public function __construct(Client $client3dbp, LoggerInterface $logger)
    {
        $this->client3dbp = $client3dbp;
        $this->logger = $logger;
    }

    public function find(SingleBinPackingRequest $request): Bin
    {
        $bins = $this->client3dbp->calculateSingleBinPacking($request);

        return $this->findBinWithAllPackedItems($request, $bins);
    }

    private function findBinWithAllPackedItems(SingleBinPackingRequest $request, array $bins): ?Bin
    {
        foreach ($bins as $bin) {
            if ($bin['not_packed_items'] === []) {
                return Bin::fromArray($bin['bin_data']);
            }
        }

        $this->logger->error(
            'No suitable bin found.',
            [
                'request' => $request,
                'bins' => $bins,
            ]
        );

        throw new SingleBinNotFound();
    }
}
