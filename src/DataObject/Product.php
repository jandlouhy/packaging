<?php

declare(strict_types=1);

namespace App\DataObject;

use Symfony\Component\Validator\Constraints as Assert;

final class Product
{
    /**
     * @var ?float
     * @Assert\NotBlank(message="Product width cannot be empty.")
     * @Assert\Type("float", message="Product width has to be float.")
     */
    private $width;

    /**
     * @var ?float
     * @Assert\NotBlank(message="Product height cannot be empty.")
     * @Assert\Type("float", message="Product width has to be float.")
     */
    private $height;

    /**
     * @var ?float
     * @Assert\NotBlank(message="Product length cannot be empty.")
     * @Assert\Type("float", message="Product width has to be float.")
     */
    private $length;

    /**
     * @var ?float
     * @Assert\NotBlank(message="Product weight cannot be empty.")
     * @Assert\Type("float", message="Product width has to be float.")
     */
    private $weight;

    public function setWidth($width): void
    {
        $this->width = $width;
    }

    public function setHeight($height): void
    {
        $this->height = $height;
    }

    public function setLength($length): void
    {
        $this->length = $length;
    }

    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    public function toArray(): array
    {
        return [
            'w' => $this->width,
            'h' => $this->height,
            'd' => $this->length,
            'wg' => $this->weight,
            'q' => 1,
        ];
    }
}
