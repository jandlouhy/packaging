<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\SingleBinNotFound;
use App\Exception\ValidationException;
use App\SingleBinPacking\SingleBinPackingRequestFactory;
use App\ThreeDBinPacking\SingleBinFinderInterface;
use App\Validator\ViolationFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

final class SingleBinPackingController
{
    private SingleBinPackingRequestFactory $singleBinPackingRequestFactory;

    private ViolationFormatter $violationFormatter;

    private SingleBinFinderInterface $singleBinFinder;

    private SerializerInterface $serializer;

    public function __construct(
        SingleBinPackingRequestFactory $singleBinPackingRequestFactory,
        ViolationFormatter $violationFormatter,
        SingleBinFinderInterface $singleBinFinder,
        SerializerInterface $serializer
    ) {
        $this->singleBinPackingRequestFactory = $singleBinPackingRequestFactory;
        $this->violationFormatter = $violationFormatter;
        $this->singleBinFinder = $singleBinFinder;
        $this->serializer = $serializer;
    }

    public function __invoke(Request $request): Response
    {
        try {
            $request = $this->singleBinPackingRequestFactory->create($request);

            $bin = $this->singleBinFinder->find($request);
        } catch (ValidationException $validationException) {
            return new JsonResponse($this->violationFormatter->format($validationException->getViolations()));
        } catch (SingleBinNotFound $exception) {
            return new JsonResponse($exception->getMessage());
        }

        return new JsonResponse(
            $this->serializer->serialize($bin, JsonEncoder::FORMAT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
