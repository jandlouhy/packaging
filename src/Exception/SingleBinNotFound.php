<?php

declare(strict_types=1);

namespace App\Exception;

final class SingleBinNotFound extends \Exception
{
    public function __construct()
    {
        parent::__construct('We do not have a single bin to pack all the products');
    }
}
