<?php

use App\Controller\SingleBinPackingController;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__ . '/.env');

$containerBuilder = new ContainerBuilder();
$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__));
$loader->load('config.yaml');

$containerBuilder->compile(true);

$request = Request::createFromGlobals();

/** @var SingleBinPackingController $handler */
$handler = $containerBuilder->get(SingleBinPackingController::class);

$response = $handler($request);
$response->send();
